# tangods-pandapcap

<img align="center" src="https://nox.apps.okd.maxiv.lu.se/widget?package=tangods-pandapcap"/>

Tango device for communicating with the PandABox PCAP (Position Capture) block and storing captured data to file.

Only Arm and Disarm is possible from this device. Other calls such as to the SEQ table for setting repeats or prescale etc should be set from the Trigger Gate controller.

If the SEQ table is used for setting a finite number of samples the capturing will stop by itself when all samples have been captured. An ongoing data capturing can also be stopped using the Disarm command.


## Repository

- Provides: `tangods-pandapcap`
- Requeriments: `pytango`, [pandapcaplib](https://gitlab.maxiv.lu.se/kits-maxiv/lib-maxiv-pandapcap/)
- Python version: 3.7+


## Properties

Name            | Default value     | Description
---             | ---               | ---
Host            | "localhost"       | Pandabox hostname or IP
TimeoutCtrl     | 2                 | Socket timeout for control (s), -1 = infinite.
TimeoutData     | -1                | Socket timeout for data (s), -1 = infinite.
AttributeList   | ""                | List of attributes (from other tango devices on the same DB)
RepublisherPort | NaN               | Port on which the data messages from the PCAP are reupublished

### AttributeList property usage
If other tango devices attributes are required to evaluate PCAP streamed values, the property usage is:
```yaml
AttributeList:   crystal=ioregister/lattice_ior_ctrl/1/Value
                 amplitude=B308A-A100830CAB03/CTL/MOCO/Amplitude
```

Both attributes `crystal` and `amplitude` will be available to be used on PCAP channel formulas. See #Channel formulas session for more details.
> **Note:** The attribute name MUST have more than 5 characters.


## Attributes

Name                | Type      | Unit  | Description
---                 | ---       | ---   | ---
destinationFilename | str       |       | Full destination hdf5 file name pattern: `<abs dir path>/<file name>.h5`
channelsToRename    | str       |       | Channels to rename. See [Channel renaming](#channel-renaming) for more details.
channelFormulas     | str       |       | Channel formulas to evaluate. See [Channel formulas](#channel-formulas) for more details.
channelNames        | [str]     |       | Channel names last received.
channelValues       | [float]   |       | Channel values last received.
nFramesReceived     | int       |       | Number of samples received.

For more details, see https://wiki.maxiv.lu.se/index.php?title=Detector_Tango_Device_Interface.

### Channel renaming

The `channelsToRename` attribute takes a string with the following syntax.

```yaml
"<current name 1>:<new name 1>;<current name 2>:<new name 2>"
```

Here is an example.
```yaml
"COUNTER1.OUT:SweetName;COUNTER2.OUT:AwesomeName"
```


### Channel formulas

The `channelFormulas` attribute takes a string with the following syntax.

```yaml
"<current name 1>:<formula 1>;<current name 2>:<formula 2>"
```

The original channel name (before any renaming) should be used. Only normal operators and functions from the `math` module are allowed in the formula. The name `value` in the formula represents the current value of the channel.
Here is an example.
```yaml
"COUNTER1.OUT:pow(value,2);COUNTER2.OUT:value*1000"
```

Also, the attributes defined on `AttributeList` property can be used to compose the formula. In this case, the attribute value is read when `Arm` command is called and replaced on the formula with the current value, e.g.:
```yaml
COUNTER1.OUT:value*amplitude;COUNTER2.OUT:value+crystal
```


## Commands

Name            | Description
---             | ---
Init            | Re-establish the connection to Pandabox.
Arm             | Start capturing data.
Disarm          | Stop capturing data.
About           | Version info for device and Panda PCAP lib and FW.


## State Machine

State           | Description
---             | ---
ON              | Connected and ready to arm.
RUNNING         | Armed and data capturing in progress.
FAULT           | Unexpected error, e.g., connection lost.


## Testing

Test requirements:
```yaml
 - pytest
 - pytest-forked
 - pytest-mock
 - pytest-cov
```

Run the test:
```bash
python -m pytest -sv --cov=pandapcap tests/test_device.py
```
