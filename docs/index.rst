.. dev-maxiv-pandapcap documentation master file, created by
   sphinx-quickstart on Thu Mar 15 16:47:29 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to dev-maxiv-pandapcap documentation!
==========================================

Contents:

.. toctree::
   :maxdepth: 2

    tangods-pandapcap <tangods-pandapcap/tangods-pandapcap>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

