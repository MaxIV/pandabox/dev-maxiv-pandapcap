import pytest
from tango import DevState
from tango.test_context import DeviceTestContext
from unittest.mock import patch, PropertyMock, MagicMock, ANY

from pandapcaplib.acquisition_config import AcquisitionConfig
from pandapcap.pandapcap import PandaPCAP
from pandapcap import __version__ as dev_version  # noqa: E402
import numpy as np


LIB_VERSION = "1.1.1"
FW_VERSION = "PandA SW: 330bd94-dirty FPGA: 0.1.9 d1275f61 00000000"
CB_HEADERS = ["name1", "name2", "name3"]
CB_DATA = [np.array([1.1, 2.2, 3.3])] * 2


## SIDE-EFFECTS


def mock_arm(config, callback):
    callback(CB_HEADERS)
    callback(CB_DATA)


## FIXTURES


@pytest.fixture
def mock_client():
    with patch("pandapcap.pandapcap.PandaPcapClient") as mock:
        mock = mock.return_value
        type(mock).lib_version = PropertyMock(return_value=LIB_VERSION)
        type(mock).fw_version = PropertyMock(return_value=FW_VERSION)
        type(mock).is_busy = PropertyMock(return_value=False)
        type(mock).num_samples_acquired = PropertyMock(return_value=10)
        mock.arm = MagicMock(side_effect=mock_arm)
        yield mock


@pytest.fixture
def mock_device(mock_client):
    client = mock_client
    props = {
        "Host": "fake",
    }
    with DeviceTestContext(PandaPCAP, properties=props) as ds:
        yield ds, client


## TESTS


@pytest.mark.forked
def test_connection(mock_device):
    ds, client = mock_device

    # Check connection OK on startup
    assert client.connect.call_count == 1
    assert ds.state() in [DevState.ON]
    assert "Device in ON state. Ready to arm." in ds.status()

    # Check connection FAIL on init
    client.connect.side_effect = Exception("Connection Error")
    ds.Init()
    assert client.connect.call_count == 2
    assert client.close.call_count == 1
    assert ds.state() == DevState.FAULT
    assert (
        "Unable to connect to PCAP at fake. Exception: Connection Error" in ds.status()
    )

    # Check connection OK again
    client.connect.side_effect = None
    ds.Init()
    assert client.connect.call_count == 3
    assert client.close.call_count == 2
    assert ds.state() in [DevState.ON]
    assert "Device in ON state. Ready to arm." in ds.status()


@pytest.mark.forked
def test_state(mock_device):
    ds, client = mock_device

    # Check connection OK on startup
    assert ds.state() in [DevState.ON]
    assert "Device in ON state. Ready to arm." in ds.status()

    # Check data captured
    type(client).is_busy = PropertyMock(return_value=True)
    assert ds.state() == DevState.RUNNING
    assert "Device in RUNNING state. Data is being captured..." in ds.status()

    # Check data capture finished
    type(client).is_busy = PropertyMock(return_value=False)
    assert ds.state() in [DevState.ON]
    assert "Device in ON state. Ready to arm." in ds.status()

    # Check error
    client.check_error.side_effect = Exception("PCAP Error")
    assert ds.state() in [DevState.FAULT]
    assert "Unexpected error, try INIT to recover. Exception: PCAP Error" in ds.status()

    # Clear error
    client.check_error.side_effect = None
    ## ...before Init, still in Failt
    assert ds.state() in [DevState.FAULT]
    assert "Unexpected error, try INIT to recover. Exception: PCAP Error" in ds.status()
    ## ...after Init, no Fault
    ds.Init()
    assert ds.state() in [DevState.ON]
    assert "Device in ON state. Ready to arm." in ds.status()


@pytest.mark.forked
def test_attributes(mock_device):
    ds, client = mock_device

    # destinationFilename RW
    value = "/tmp/filename.h5"
    ds.destinationFilename = value
    assert ds.destinationFilename == value

    # channelsToRename RW
    ## Syntax OK
    value = "old_name_1:new_name_1;old_name_2:new_name_2"
    ds.channelsToRename = value
    assert ds.channelsToRename == value
    ## Syntax invalid 1
    value = "old_name_1=new_name_1,old_name_2=new_name_2"
    with pytest.raises(Exception) as e:
        ds.channelsToRename = value
    assert (
        'Invalid channel renaming string, use syntax: "<old1>:<new1>;<old2>:<new2>"'
        in str(e.value)
    )
    ## Syntax invalid 2
    value = "blabla"
    with pytest.raises(Exception) as e:
        ds.channelsToRename = value
    assert (
        'Invalid channel renaming string, use syntax: "<old1>:<new1>;<old2>:<new2>"'
        in str(e.value)
    )
    ## Clear value
    value = ""
    ds.channelsToRename = value
    assert ds.channelsToRename == value

    # channelFormulas RW
    ## Syntax OK
    value = "old_name_1:sqrt(value);old_name_2:value*1000"
    ds.channelFormulas = value
    assert ds.channelFormulas == value
    ## Syntax invalid 1
    value = "old_name_1=sqrt(value),old_name_2=value*1000"
    with pytest.raises(Exception) as e:
        ds.channelFormulas = value
    assert (
        'Invalid channel formula string, use syntax: "<ch1>:<formula1>;<ch2>:<formula2>"'
        in str(e.value)
    )
    ## Syntax invalid 2
    value = "blabla"
    with pytest.raises(Exception) as e:
        ds.channelFormulas = value
    assert (
        'Invalid channel formula string, use syntax: "<ch1>:<formula1>;<ch2>:<formula2>"'
        in str(e.value)
    )
    ## Clear value
    value = ""
    ds.channelFormulas = value
    assert ds.channelFormulas == value

    # channelNames R
    ds.Arm()
    assert ds.channelNames == tuple(CB_HEADERS)
    # channelValues R
    assert np.allclose(ds.channelValues, np.array([float(val[-1]) for val in CB_DATA]))

    # nFramesReceived R
    assert ds.nFramesReceived == 10


@pytest.mark.forked
def test_commands(mock_device):
    ds, client = mock_device

    filename = "/tmp/filename.h5"
    rename_str = "old_name_1:new_name_1;old_name_2:new_name_2"
    rename_dict = {
        "old_name_1": "new_name_1",
        "old_name_2": "new_name_2",
    }
    formula_str = "old_name_1:sqrt(value);old_name_2:value*1000"
    formula_dict = {
        "old_name_1": "sqrt(value)",
        "old_name_2": "value*1000",
    }
    ds.destinationFilename = filename
    ds.channelsToRename = rename_str
    ds.channelFormulas = formula_str
    config = AcquisitionConfig(
        filename,
        rename_dict,
        formula_dict,
    )
    type(client).is_busy = PropertyMock(return_value=False)
    assert ds.state() in [DevState.ON]

    # Arm
    ds.Arm()
    client.arm.assert_called_with(config, callback=ANY)
    type(client).is_busy = PropertyMock(return_value=True)
    assert ds.state() in [DevState.RUNNING]

    # Disarm
    client.disarm.reset_mock()
    ds.Disarm()
    client.disarm.assert_called_once()
    type(client).is_busy = PropertyMock(return_value=False)
    assert ds.state() in [DevState.ON]

    # About
    assert ds.About() == (
        f"Device version: {dev_version}\n"
        + f"Library version: {LIB_VERSION}\n"
        + f"Firmware version: {FW_VERSION}"
    )
