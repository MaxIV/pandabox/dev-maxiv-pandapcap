#!/usr/bin/env python

###############################################################################
#     tangods-pandapcap
#
#     Copyright (C) 2021  MAX IV Laboratory, Lund Sweden.
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see [http://www.gnu.org/licenses/].
###############################################################################

from setuptools import setup, find_packages


setup(
    name="tangods-pandapcap",
    use_scm_version=True,
    setup_requires=["setuptools_scm"],
    description="Tango device for communicating with the PandaBox PCAP block and storing captured data to file.",
    author="KITS",
    author_email="kits-sw@maxiv.lu.se",
    license="GPL-3.0-or-later",
    url="https://gitlab.maxiv.lu.se/kits-maxiv/dev-maxiv-pandapcap",
    entry_points={
        "console_scripts": [
            "PandaPCAP=pandapcap.pandapcap:main",
        ]
    },
    packages=find_packages(exclude=("tests", "tests.*")),
    python_requires=">=3.7",
    install_requires=["pytango", "pandapcaplib>=1.1.0"],
)
