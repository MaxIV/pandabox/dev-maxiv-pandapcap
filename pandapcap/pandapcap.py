#!/usr/bin/env python

###############################################################################
#     PandaPCAP
#
#     Copyright (C) 2020  MAX IV Laboratory, Lund Sweden.
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see [http://www.gnu.org/licenses/].
###############################################################################
from functools import wraps
import re

from tango import (
    DevState,
    AttrWriteType,
    AttReqType,
    DevString,
    DevLong,
    DevFloat,
    DevVarStringArray,
    AttributeProxy,
    DevFailed,
)
from tango.server import Device, attribute, command, device_property
import numpy as np
from pandapcaplib.pandapcap_client import PandaPcapClient
from pandapcaplib.acquisition_config import AcquisitionConfig
from pandapcaplib.path_validator import PathValidationException
from pandapcaplib.evaluate_formula import EvaluateFormulaException

from pandapcap import __version__ as version


def handle_error(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        try:
            return func(self, *args, **kwargs)
        except PathValidationException as e:
            err_msg = f"Invalid file path, {e}"
            self.warn_stream(err_msg)
            raise RuntimeError(err_msg)
        except (InvalidAttrValue, EvaluateFormulaException) as e:
            err_msg = str(e)
            self.warn_stream(err_msg)
            raise RuntimeError(err_msg)
        except Exception as e:
            err_msg = f"Unable to execute {func.__name__}. Exception: {e}"
            self.set_state(DevState.FAULT)
            self.set_status(err_msg)
            self.error_stream(err_msg)
            raise RuntimeError(err_msg)

    return wrapper


def debug_log(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        self.debug_stream(f"-> Entering {func.__name__} with args: {args}")
        output = func(self, *args, **kwargs)
        self.debug_stream(f"<- Exiting {func.__name__} with output: {output}")
        return output

    return wrapper


class InvalidAttrValue(Exception):
    """Exception for invalid attribute value"""


class PandaPCAP(Device):

    # ----------------------------- #
    # ----- device properties ----- #
    # ----------------------------- #

    Host = device_property(
        dtype=str, doc="Pandabox host address", default_value="localhost"
    )

    TimeoutCtrl = device_property(
        dtype=float,
        doc="Socket timeout for control (s), -1 = infinite",
        default_value=2.0,
    )

    TimeoutData = device_property(
        dtype=float,
        doc="Socket timeout for data (s), -1 = infinite",
        default_value=-1.0,
    )

    RepublisherPort = device_property(
        dtype=int,
        doc="Port on which the messages should be republished",
        default_value=None,
    )

    AttributeList = device_property(
        dtype=DevVarStringArray,
        default_value=[],
        doc="List of attributes to be used to evaluate PCAP values",
    )

    # ----------------------------- #
    # ----- device attributes ----- #
    # ----------------------------- #

    destinationFilename = attribute(
        label="Destination filename",
        dtype=DevString,
        access=AttrWriteType.READ_WRITE,
        doc="Full destination hdf5 file pattern: <abs dir path>/<file name>.h5",
        memorized=False,
        hw_memorized=False,
        fisallowed="is_attr_allowed",
    )

    channelsToRename = attribute(
        label="Channels to rename",
        dtype=DevString,
        access=AttrWriteType.READ_WRITE,
        doc="Channels to rename (; separated): <current name>:<new name>",
        memorized=True,
        hw_memorized=True,
        fisallowed="is_attr_allowed",
    )

    channelFormulas = attribute(
        label="Channel formulas",
        dtype=DevString,
        access=AttrWriteType.READ_WRITE,
        doc="Channel formulas to evaluate (; separated): <current name>:<formula>",
        memorized=True,
        hw_memorized=True,
        fisallowed="is_attr_allowed",
    )

    channelNames = attribute(
        label="Channel names",
        dtype=[DevString],
        max_dim_x=1000,
        access=AttrWriteType.READ,
        doc="Channel names (renamed) last received",
        fisallowed="is_attr_allowed",
    )

    channelValues = attribute(
        label="Channel values",
        dtype=[DevFloat],
        access=AttrWriteType.READ,
        max_dim_x=1000,
        doc="Channel values last received",
        fisallowed="is_attr_allowed",
    )

    nFramesReceived = attribute(
        label="Number of samples received",
        dtype=DevLong,
        access=AttrWriteType.READ,
        doc="Number of samples received",
        fisallowed="is_attr_allowed",
    )

    zmqSubscriptionAddr = attribute(
        label="Address of ZMQ republisher",
        dtype=DevString,
        access=AttrWriteType.READ,
    )

    # ----------------------------- #
    # ----- attribute methods ----- #
    # ----------------------------- #

    def is_attr_allowed(self, request):
        """Default check for attributes"""
        self._update_state_status()
        if request == AttReqType.WRITE_REQ:
            return self.get_state() == DevState.ON
        else:
            return self.get_state() not in [DevState.INIT, DevState.FAULT]

    @handle_error
    @debug_log
    def read_destinationFilename(self):
        """Returns the full file name pattern"""
        return self._destination_filename

    @handle_error
    @debug_log
    def write_destinationFilename(self, filename):
        """Sets the full file name pattern"""
        self._destination_filename = filename

    @handle_error
    @debug_log
    def read_channelsToRename(self):
        """Returns the channels to rename"""
        return ";".join(
            [f"{key}:{value}" for key, value in self._channel_renaming.items()]
        )

    @handle_error
    @debug_log
    def write_channelsToRename(self, channels):
        """Sets the channels to rename"""
        channels = channels.strip()
        if channels:
            self._channel_renaming = self._parse_channel_renaming(channels)
        else:
            # Clear dict if empty
            self._channel_renaming = {}

    @handle_error
    @debug_log
    def read_channelFormulas(self):
        """Returns the channel formulas"""
        return ";".join(
            [f"{key}:{value}" for key, value in self._channel_formula.items()]
        )

    @handle_error
    @debug_log
    def write_channelFormulas(self, channels):
        """Sets the channel formulas"""
        channels = channels.strip()
        if channels:
            self._channel_formula = self._parse_channel_formulas(channels)
        else:
            # Clear dict if empty
            self._channel_formula = {}

    @handle_error
    @debug_log
    def read_channelNames(self):
        """Returns the channel names last captured"""
        return self._channel_names

    @handle_error
    @debug_log
    def read_channelValues(self):
        """Returns the channel values last captured"""
        return self._channel_values

    @handle_error
    @debug_log
    def read_nFramesReceived(self):
        """Returns the number of samples received"""
        return self.client.num_samples_acquired

    @handle_error
    @debug_log
    def read_zmqSubscriptionAddr(self):
        """Returns the subscription address for the republisher"""
        return self.client.zmq_republisher_addr

    # --------------------------- #
    # ----- command methods ----- #
    # --------------------------- #

    @debug_log
    def is_Arm_allowed(self):
        """Check if PCAP is ready to capture data"""
        self._update_state_status()
        return self.get_state() == DevState.ON

    @command
    @handle_error
    @debug_log
    def Arm(self):
        """Start capturing data"""
        channel_formula_updated = self._update_channel_formula()
        self._channel_names = []
        self._channel_values = []
        config = AcquisitionConfig(
            self._destination_filename, self._channel_renaming, channel_formula_updated
        )
        self.client.arm(config, callback=self._data_received)

    @command
    @handle_error
    @debug_log
    def Disarm(self):
        """Stop capturing data"""
        self.client.disarm()

    @command(dtype_out=str)
    @debug_log
    def About(self):
        """Returns the tango device, library and PCAP fw versions"""
        return (
            f"Device version: {self.device_version}\n"
            + f"Library version: {self._pcap_info['lib_version']}\n"
            + f"Firmware version: {self._pcap_info['fw_version']}"
        )

    # -------------------------- #
    # ----- device methods ----- #
    # -------------------------- #

    @debug_log
    def init_device(self):
        Device.init_device(self)
        self._destination_filename = ""
        self.attr_dict = {}
        self._channel_renaming = {}
        self._channel_formula = {}
        self._channel_names = []
        self._channel_values = []
        self._pcap_info = {
            "lib_version": "NA",
            "fw_version": "NA",
        }
        try:
            if self.TimeoutCtrl < 0:
                self.TimeoutCtrl = None
            if self.TimeoutData < 0:
                self.TimeoutData = None
            self._initialize_attributes_dict()
            self.client = PandaPcapClient(
                self.Host, self.TimeoutCtrl, self.TimeoutData, self.RepublisherPort
            )
            self.client.connect()

            # the device is disarmed on init_device to guarantee any open file is closed/released and the
            # device is in a good state (DevState.ON) when `set_memorized_attributes` method is called (after init is finished)
            # if PCAP remains armed when init_device method is called, the memorized attributes will fail to be written and the device
            # will be set to DevState.ALARM. the memorized attributes are important to handle datasets renaming and formulas.
            self.client.disarm()

            self._read_pcap_info()
            self.set_state(DevState.ON)
            self.set_status("Device in ON state. Ready to arm.")
            self.info_stream(f"Connected to PCAP at {self.Host}.")
        except Exception as e:
            err_msg = f"Unable to connect to PCAP at {self.Host}. Exception: {e}"
            self.set_state(DevState.FAULT)
            self.set_status(err_msg)
            self.error_stream(err_msg)

    @debug_log
    def delete_device(self):
        try:
            self.client.disarm()
        except Exception as e:
            self.error_stream(str(e))
        try:
            self.client.close()
        except Exception as e:
            self.error_stream(str(e))
        self.info_stream(f"Disconnected from PCAP at {self.Host}.")

    def dev_state(self):
        self._update_state_status()
        return self.get_state()

    def dev_status(self):
        self._update_state_status()
        return self.get_status()

    @debug_log
    def _update_state_status(self):
        """Update state and status based on connection status"""
        try:
            current_state = self.get_state()
            if current_state not in [DevState.INIT, DevState.FAULT]:
                self.client.check_error()
                if self.client.is_busy:
                    self.set_state(DevState.RUNNING)
                    self.set_status(
                        "Device in RUNNING state. Data is being captured..."
                    )
                else:
                    self.set_state(DevState.ON)
                    self.set_status("Device in ON state. Ready to arm.")
        except Exception as e:
            self.set_state(DevState.FAULT)
            self.set_status(f"Unexpected error, try INIT to recover. Exception: {e}")

    @property
    def device_version(self):
        """Get tango device version"""
        return version

    def _read_pcap_info(self):
        """Reads the PCAP library and FW versions"""
        self._pcap_info["lib_version"] = self.client.lib_version
        self._pcap_info["fw_version"] = self.client.fw_version

    def _parse_channel_renaming(self, channels: str) -> dict:
        """Parse channel renaming string to dict"""
        ch_dict = {}
        for ch in channels.split(";"):
            try:
                old, new = tuple(ch.strip().split(":"))
                ch_dict[old.strip()] = new.strip()
            except Exception:
                raise InvalidAttrValue(
                    'Invalid channel renaming string, use syntax: "<old1>:<new1>;<old2>:<new2>"'
                )
        return ch_dict

    def _parse_channel_formulas(self, channels: str) -> dict:
        """Parse channel formulas string to dict"""
        ch_dict = {}
        for ch in channels.split(";"):
            try:
                name, formula = tuple(ch.strip().split(":"))
                ch_dict[name.strip()] = formula.strip()
            except Exception:
                raise InvalidAttrValue(
                    'Invalid channel formula string, use syntax: "<ch1>:<formula1>;<ch2>:<formula2>"'
                )
        return ch_dict

    def _data_received(self, data):
        """Data callback method for reading received channel names and values"""
        if data:
            if isinstance(data[0], str):  # Names
                self._channel_names = data
            elif isinstance(data[0], np.ndarray):  # Values
                self._channel_values = [float(val[-1]) for val in data]
        else:
            self.warn_stream("Data received is empty")

    def _initialize_attributes_dict(self):
        """Basic syntax check of the attributes defined in AttributeList property"""
        if self.AttributeList:
            attr_name = []
            attr_full = []
            for row in self.AttributeList:
                attr_n = row.split("=")[0].strip('"').lower()
                attr_f = row.split("=")[1].strip('"').lower()
                if len(attr_n) >= 5:
                    try:
                        AttributeProxy(attr_f)
                        attr_name.append(attr_n)
                        attr_full.append(attr_f)
                    except DevFailed as e:
                        # handle wrong syntax errors. try to avoid the use of tango devices
                        # not defined on the DB or syntax mistakes when defining an attribute
                        reason = str(e.args[0].reason)
                        if reason in [
                            "DB_DeviceNotDefined",
                            "API_UnsupportedAttribute",
                        ]:
                            raise ValueError(
                                f"PCAP device cannot be initialized. The attribute name {attr_n} has wrong syntax. Reason: {reason}"
                                f"Edit the attribute {attr_n} with a valid device/attribute name and restart PCAP device again."
                            )
                else:
                    raise ValueError(
                        "PCAP device cannot be initialized. The attribute name on AttributeList"
                        f"property must have at least 5 characters. Edit {attr_n} and restart the device again."
                    )
            self.attr_dict = dict(zip(attr_name, attr_full))

    def _update_channel_formula(self):
        """Replaces the attribute name defined in AttributeList property by read values,
        if any attribute is being used to evaluate a channel value"""
        if self.attr_dict:  # avoid empty dict to be executed
            ch_formula_attr = {}
            attr_value = {}
            for k in self._channel_formula.keys():
                for attr in self.attr_dict.keys():
                    if bool(
                        re.search(
                            rf"\b{attr}\b", self._channel_formula[k], re.IGNORECASE
                        )
                    ):  # only read attribute if it is being used in any formula
                        attr_str = self.attr_dict[attr]
                        if attr_str not in attr_value.keys():
                            try:
                                attr_value[attr_str] = (
                                    AttributeProxy(attr_str).read().value
                                )
                            except DevFailed as e:
                                dev = AttributeProxy(attr_str).get_device_proxy()
                                name = AttributeProxy(attr_str).name()
                                reason = str(e.args[0].reason)
                                if reason == "API_CantConnectToDevice":
                                    raise RuntimeError(
                                        f"Failed to connect to device {dev}. Attribute {name} value cannot be evaluated."
                                        "Check if the device server is running in Astor."
                                    )
                                else:
                                    self.error_stream(
                                        f"Exception when trying to read {attr_str} value. Full message {e}"
                                    )
                                    raise RuntimeError(
                                        f"Unexpected exception when trying to read {attr_str} value."
                                        f"Reason: {reason}. Check device logs for full error message."
                                    )

                        ch_formula_attr[k] = re.sub(
                            rf"\b{attr}\b",
                            str(attr_value[attr_str]),
                            self._channel_formula[k],
                        )
            return ch_formula_attr
        else:
            return self._channel_formula


# -------------------------------------------------------------------------


def main(args=None):

    import argparse
    import logging

    parser = argparse.ArgumentParser()
    parser.add_argument("--log-level", default="info")
    args, unknown = parser.parse_known_args(args)
    logging.basicConfig(
        format="%(asctime)s %(levelname)s %(name)s %(message)s",
        level=args.log_level.upper(),
    )

    PandaPCAP.run_server()


if __name__ == "__main__":
    main()
